#ifndef SIMPLE_SUPPORT_TUPLE_UTILS_META_TRANSFORM_HPP
#define SIMPLE_SUPPORT_TUPLE_UTILS_META_TRANSFORM_HPP

#include "meta_flatten.hpp"

namespace simple::support
{

	template <template<typename...> typename transformer>
	struct meta_transform_tuple_wrapper
	{
		template <typename... Ts>
		struct op
		{
			using type = std::tuple<transformer<Ts...>>;
		};
	};

	template <typename Tuple, template <typename...> typename Operator>
	using transform_t = flatten_t
	<
		Tuple,
		meta_transform_tuple_wrapper<Operator>::template op
	>;

} // namespace simple::support

#endif /* end of include guard */

#include "simple/support/carcdr.hpp"

using namespace simple::support;

template <int... Is>
using iseq = std::integer_sequence<int, Is...>;

int main()
{
	using ints = lisp_list<int, -1, 10, 13, 999>;
	static_assert(car<ints> == -1);
	static_assert(std::is_same_v< cdr<ints>, lisp_list<int, 10, 13, 999> >);
	static_assert(car< cdr<cdr<ints>> > == 13);
	static_assert(car<ints, 3> == 999);
	static_assert(car<lisp_list<int>, 0, 321> == 321);
	static_assert(car<ints, 10, -9517> == -9517); // *shrug*

	static_assert(std::is_same_v<cdr<cdr<cdr<cdr<ints>>>>, lisp_list<int>>);


	static_assert(std::is_same_v<
		concat_seq_t<iseq<1,2>, iseq<3,4,5>>,
		iseq<1,2,3,4,5>>);

	static_assert(std::is_same_v<
		offset_seq_t<int, -3, iseq<1,2,3>>,
		iseq<-2,-1,0>>);

	return 0;
}
